const { 
    getSumOfMatchers,
    getSumOf50Percenters,
    getNextPositionNumber
} = require('./day1')

/* getSumOf50Percenters
---------------------------------------- */
it('1122 produces a sum of 3 (1 + 2)', () => {
    expect(getSumOfMatchers('1122')).toBe(3)
})

it('1111 produces 4', () => {
    expect(getSumOfMatchers('1111')).toBe(4)
})

it('1234 produces 0', () => {
    expect(getSumOfMatchers('1234')).toBe(0)
})

it('91212129 produces 9', () => {
    expect(getSumOfMatchers('91212129')).toBe(9)
})

/* getSumOf50Percenters
---------------------------------------- */

// getNextPosition
it('index 1 of [1,2,3,4] should return 4', () => {
    expect(
        getNextPositionNumber(1, [1,2,3,4])
    ).toBe(4)
})

it('index 2 of [1,2,3,4] should return 1', () => {
    expect(
        getNextPositionNumber(2, [1,2,3,4])
    ).toBe(1)
})

// getSumOf50Percenters
it('1212 produces 6', () => {
    expect(getSumOf50Percenters('1212')).toBe(6)
})

it('1221 produces 0', () => {
    expect(getSumOf50Percenters('1221')).toBe(0)
})

it('123425 produces 4', () => {
    expect(getSumOf50Percenters('123425')).toBe(4)
})

it('123123 produces 12', () => {
    expect(getSumOf50Percenters('123123')).toBe(12)
})

it('12131415 produces 4', () => {
    expect(getSumOf50Percenters('12131415')).toBe(4)
})

